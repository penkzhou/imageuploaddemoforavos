package com.penkzhou.imageuploaddemoforavos.app.adapter;

/**
 * Created by Administrator on 14-3-3.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.penkzhou.imageuploaddemoforavos.app.R;
import com.penkzhou.imageuploaddemoforavos.app.model.CropOption;

import java.util.ArrayList;


public class CropOptionAdapter extends ArrayAdapter<CropOption> {
    private ArrayList<CropOption> mOptions;
    private LayoutInflater mInflater;

    public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
        super(context, R.layout.crop_selector, options);

        mOptions = options;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup group) {
        final ViewHolder viewHolder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.crop_selector, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) v.findViewById(R.id.tv_name);
            viewHolder.icon = (ImageView) v.findViewById(R.id.iv_icon);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        final CropOption item = mOptions.get(position);

        if (item != null) {
            viewHolder.icon.setImageDrawable(item.icon);
            viewHolder.name.setText(item.title);
        }

        return v;
    }

    public static class ViewHolder {
        public TextView name;
        public ImageView icon;

    }
}
