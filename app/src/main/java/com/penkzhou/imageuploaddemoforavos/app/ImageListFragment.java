package com.penkzhou.imageuploaddemoforavos.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.penkzhou.imageuploaddemoforavos.app.adapter.ImageListAdapter;
import com.penkzhou.imageuploaddemoforavos.app.model.ImageObject;

import java.util.List;

/**
 * Created by Administrator on 2014/4/14.
 */
public class ImageListFragment extends Fragment{


    MainActivity mParent;
    ProgressDialog pdl;
    private ImageListAdapter imageListAdapter;
    private GridView imageList;

    public ImageListFragment() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image_list, container, false);
        imageList = (GridView) rootView.findViewById(R.id.gv_image_list);
        pdl = new ProgressDialog(getActivity());
        pdl.setTitle("加载");
        pdl.setMessage("图片加载中，请稍候…");
        loadImageList();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = (MainActivity) activity;

    }





    public void loadImageList() {
        AVQuery<ImageObject> query = new AVQuery<ImageObject>("ImageObject");
        query.orderByDescending("createdAt");
        pdl.show();
        query.findInBackground(new FindCallback<ImageObject>() {
            public void done(List<ImageObject> avObjects, AVException e) {
                pdl.dismiss();
                if (e == null) {
                    if (avObjects.size() == 0) {
                    } else {
                        imageListAdapter = new ImageListAdapter(getActivity(),avObjects);
                        imageList.setAdapter(imageListAdapter);
                    }
                } else {
                    Log.d("ImageListFragment", "loadImageList()失败,查询错误: " + e.getCode()+e.getMessage());
                }
            }
        });
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}