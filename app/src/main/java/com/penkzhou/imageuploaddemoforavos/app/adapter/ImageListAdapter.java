package com.penkzhou.imageuploaddemoforavos.app.adapter;

/**
 * Created by Administrator on 14-3-3.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.penkzhou.imageuploaddemoforavos.app.R;
import com.penkzhou.imageuploaddemoforavos.app.model.ImageObject;
import com.squareup.picasso.Picasso;

import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Adapter for crop option list.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
public class ImageListAdapter extends ArrayAdapter<ImageObject> {
    private List<ImageObject> objects;
    private LayoutInflater mInflater;
    private PhotoViewAttacher mAttacher;

    public ImageListAdapter(Context context, List<ImageObject> objects) {
        super(context, R.layout.image_item, objects);

        this.objects = objects;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup group) {
        View v = convertView;
        final ViewHolder viewHolder;
        if (v == null) {
            v = mInflater.inflate(R.layout.image_item, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) v.findViewById(R.id.tv_title);
            viewHolder.image = (ImageView) v.findViewById(R.id.iv_image);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        final ImageObject imageObject = objects.get(position);
        final AVFile avatarFile = (AVFile) imageObject.getImageFile();

        if (imageObject != null) {
            viewHolder.title.setText(imageObject.getTitle());
            Picasso.with(getContext()).load(avatarFile.getUrl()).placeholder(R.drawable.placeholder).into(viewHolder.image);
        }
        return v;
    }

    public static class ViewHolder {
        public TextView title;
        public ImageView image;

    }
}
