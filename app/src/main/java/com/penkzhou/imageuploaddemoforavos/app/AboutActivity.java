package com.penkzhou.imageuploaddemoforavos.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;


/**
 * Created by Administrator on 14-3-14.
 */
public class AboutActivity extends ActionBarActivity {
    private TextView appname, version, author, source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        appname = (TextView) findViewById(R.id.tv_about_appname);
        version = (TextView) findViewById(R.id.tv_about_version);
        author = (TextView) findViewById(R.id.tv_about_author);
        source = (TextView) findViewById(R.id.tv_about_source);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return false;
    }
}
