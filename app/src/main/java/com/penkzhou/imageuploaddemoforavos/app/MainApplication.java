package com.penkzhou.imageuploaddemoforavos.app;

import android.app.Application;
import android.widget.ImageButton;

import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.PushService;
import com.penkzhou.imageuploaddemoforavos.app.model.ImageObject;

/**
 * Created by Administrator on 2014/4/14.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AVOSCloud.useAVCloudCN();
        AVObject.registerSubclass(ImageObject.class);
        AVOSCloud.initialize(this, "rfv7ahwtg8libnyvmfyyw7wbdpprb1cxcz5cku3vz00fqvq7", "utak7kk8l9n45jfb6n3dqe6xja68e32qa19cn0b8zkgipn35");
        AVInstallation.getCurrentInstallation().saveInBackground();
        PushService.setDefaultPushCallback(this, MainActivity.class);
    }
}
