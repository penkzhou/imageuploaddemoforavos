package com.penkzhou.imageuploaddemoforavos.app.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;

/**
 * Created by Administrator on 2014/4/14.
 */
@AVClassName("ImageObject")
public class ImageObject extends AVObject {

    public final static String TITLE = "title";
    public final static String IMAGEFILE = "imageFile";

    public void setTitle(String title){
        put(TITLE,title);
    }
    public String getTitle(){
        return getString(TITLE);
    }


    public void setImageFile(AVFile imageFile){
        put(IMAGEFILE,imageFile);
    }
    public AVFile getImageFile(){
        return getAVFile(IMAGEFILE);
    }
}
